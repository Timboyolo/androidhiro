# README #


### AR on Android ###

* Augmented reality
* Unity
* Android
* ARToolkit

### Initial setup ###

ARToolkit for Unity Android build Howto V1.0

Benodigtheden:
-	Android Telefoon met developer settings en installatie uit onbekende bronnen
-	Data kabel
-	Unity installation (personal)
-	ADB Driver voor je gsm (niet nodig voor Win10)

Stap 1 maak een project:

-	Download & Unpack ARtoolkit naar een terug te vinden locatie
https://artoolkit.org/documentation/doku.php?id=6_Unity:unity_about

-	Volg dit filmpje voor een initiële setup van het project 
https://www.youtube.com/watch?v=T8O-XKQ2Avo
Nu is het mogelijk het project te runnen met de webcam van je pc en zou hij in staat moeten zijn de 3D-kubus boven de tag te renderen.
 
 
Stap 2 Build voor android:

-	Browse naar 
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
en download de nieuwste stabiele versie van de Java-JDK voor je systeem.
 

-	Browse naar
https://developer.android.com/studio/index.html#Other
Sroll naar beneden en download de SDK, Android studio is optioneel hier.
 
 
-	Ga in Unity naar Edit > Preferences > External tools en selecteer de plek van je Android-SDK installatie en daaronder de referentie naar je Java-JDK locatie.
 

-	Druk op Ctrl + Shift + B, add de open scene, switch platform naar android en klik op player settings.
  

-	Het enige dat in de player settings veranderd moet worden is de bundle identifier. Kies hiervoor een goede naar en kopiëer hem.
 
-	Ga naar Assets > Android > Plugins, open de file AndroidManifest.XML in je favoriete tekst editor en verander hier ook de package naam naar dezelfde naam als de bundle identifier.
 

 

Stap 3 Upload & Run:

-	Connecteer Het Android-Toestel en druk in Unity Ctrl+B, Kies een naam voor de APK en save. Als alles goed is zal hij de APK ook installeren op je Android device en kan je de app nu gebruiken!


### Contact ###

* tim.djoos@intation.eu