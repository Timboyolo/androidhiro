﻿using UnityEngine;
using System.Collections;

public class toggleLights : MonoBehaviour {

    private Light[] lights;
    public GameObject StreetLights;
    public GameObject StreetLight;
    [SerializeField]
    private bool lightsOnOff = false;
    // Use this for initialization
    void Start () {

        lights = StreetLights.GetComponentsInChildren<Light>(true);
        if (!lightsOnOff)
        {
            foreach (Light light in lights)
                {
                    light.enabled = false;
                }
        }
        else
        {
            foreach (Light light in lights)
            {
                light.enabled = true;
            }
        }
}
	
	// Update is called once per frame
	void Update () {
	
	}
}
