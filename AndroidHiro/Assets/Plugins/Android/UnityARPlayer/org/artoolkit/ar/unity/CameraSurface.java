/*
 * Decompiled with CFR 0_115.
 * 
 * Could not load the following classes:
 *  android.content.Context
 *  android.graphics.PixelFormat
 *  android.hardware.Camera
 *  android.hardware.Camera$CameraInfo
 *  android.hardware.Camera$Parameters
 *  android.hardware.Camera$PreviewCallback
 *  android.hardware.Camera$Size
 *  android.os.Build
 *  android.os.Build$VERSION
 *  android.preference.PreferenceManager
 *  android.util.Log
 *  android.view.SurfaceHolder
 *  android.view.SurfaceHolder$Callback
 *  android.view.SurfaceView
 *  org.artoolkit.ar.base.NativeInterface
 */
package org.artoolkit.ar.unity;

import android.content.Context;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.io.IOException;
import org.artoolkit.ar.base.NativeInterface;

public class CameraSurface
extends SurfaceView
implements SurfaceHolder.Callback,
Camera.PreviewCallback {
    private static final String TAG = "CameraSurface";
    private Camera camera;
    private int mWidth = 0;
    private int mHeight = 0;
    private boolean mCameraIsFrontFacing = false;
    private int mCameraIndex = 0;

    static {
        NativeInterface.loadNativeLibrary();
    }

    public CameraSurface(Context context) {
        super(context);
        SurfaceHolder holder = this.getHolder();
        holder.addCallback((SurfaceHolder.Callback)this);
        holder.setType(3);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        Log.i((String)"CameraSurface", (String)"Opening camera.");
        try {
            if (Build.VERSION.SDK_INT >= 9) {
                int cameraIndex = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences((Context)this.getContext()).getString("pref_cameraIndex", "0"));
                this.camera = Camera.open((int)cameraIndex);
            } else {
                this.camera = Camera.open();
            }
        }
        catch (RuntimeException exception) {
            Log.e((String)"CameraSurface", (String)"Cannot open camera. It may be in use by another process.");
        }
        if (this.camera != null) {
            try {
                this.camera.setPreviewDisplay(holder);
                this.camera.setPreviewCallbackWithBuffer((Camera.PreviewCallback)this);
            }
            catch (IOException exception) {
                Log.e((String)"CameraSurface", (String)"Cannot set camera preview display.");
                this.camera.release();
                this.camera = null;
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.camera != null) {
            Log.i((String)"CameraSurface", (String)"Closing camera.");
            this.camera.stopPreview();
            this.camera.setPreviewCallback(null);
            this.camera.release();
            this.camera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        if (this.camera != null) {
            String camResolution = PreferenceManager.getDefaultSharedPreferences((Context)this.getContext()).getString("pref_cameraResolution", "320x240");
            String[] dims = camResolution.split("x", 2);
            Camera.Parameters parameters = this.camera.getParameters();
			parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            parameters.setPreviewSize(Integer.parseInt(dims[0]), Integer.parseInt(dims[1]));
            parameters.setPreviewFrameRate(30);
            this.camera.setParameters(parameters);
            parameters = this.camera.getParameters();
            int capWidth = parameters.getPreviewSize().width;
            int capHeight = parameters.getPreviewSize().height;
            int pixelformat = parameters.getPreviewFormat();
            PixelFormat pixelinfo = new PixelFormat();
            PixelFormat.getPixelFormatInfo((int)pixelformat, (PixelFormat)pixelinfo);
            int cameraIndex = 0;
            boolean frontFacing = false;
            if (Build.VERSION.SDK_INT >= 9) {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                cameraIndex = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences((Context)this.getContext()).getString("pref_cameraIndex", "0"));
                Camera.getCameraInfo((int)cameraIndex, (Camera.CameraInfo)cameraInfo);
                if (cameraInfo.facing == 1) {
                    frontFacing = true;
                }
            }
            int bufSize = capWidth * capHeight * pixelinfo.bitsPerPixel / 8;
            Log.i((String)"CameraSurface", (String)("Camera buffers will be " + capWidth + "x" + capHeight + "@" + pixelinfo.bitsPerPixel + "bpp, " + bufSize + "bytes."));
            int i = 0;
            while (i < 5) {
                this.camera.addCallbackBuffer(new byte[bufSize]);
                ++i;
            }
            this.mWidth = capWidth;
            this.mHeight = capHeight;
            this.mCameraIndex = cameraIndex;
            this.mCameraIsFrontFacing = frontFacing;
            this.camera.startPreview();
        }
    }

    public void onPreviewFrame(byte[] data, Camera cam) {
        NativeInterface.arwAcceptVideoImage((byte[])data, (int)this.mWidth, (int)this.mHeight, (int)this.mCameraIndex, (boolean)this.mCameraIsFrontFacing);
        cam.addCallbackBuffer(data);
    }
}