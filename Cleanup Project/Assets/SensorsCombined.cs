﻿using UnityEngine;
using System.Collections;

public class SensorsCombined : MonoBehaviour
{
    Vector3 accelero;
    Vector3 fly;
    public float thrust;
    void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
    // Use this for initialization
    void Start()
    {
        if (!Input.gyro.enabled)
        {
            Input.gyro.enabled = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        accelero = new Vector3(-Input.acceleration.x / 2, -Input.acceleration.y / 2, -Input.acceleration.z / 2);
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f) * new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
        if (Input.anyKey)
        {
            fly += transform.forward * thrust * Time.deltaTime;
        }
        transform.position = fly + accelero;
    }
}