﻿using UnityEngine;
using System.Collections;

public class CylinderTwister : MonoBehaviour {

    public float m_Speed = 20f;

    private Vector3 m_RotationDirection3 = Vector3.up;
    private Vector3 m_RotationDirection4 = Vector3.left;

    public void ToggleRotationDirection()
    {
        Debug.Log("Toggling rotation direction");

        if (m_RotationDirection3 == Vector3.up)
        {
            m_RotationDirection3 = Vector3.down;
        }
        else
        {
            m_RotationDirection3 = Vector3.up;
        }
        if (m_RotationDirection4 == Vector3.left)
        {
            m_RotationDirection4 = Vector3.right;
        }
        else
        {
            m_RotationDirection4 = Vector3.left;
        }
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            ToggleRotationDirection();
        }
        transform.Rotate(m_RotationDirection3 * Time.deltaTime * m_Speed);
        transform.Rotate(m_RotationDirection4 * Time.deltaTime * m_Speed);
    }
}
