﻿using UnityEngine;
using System.Collections;

public class DebugText : MonoBehaviour {
    public GameObject t;
    float deltaTime = 0.0f;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        TextMesh textMesh = t.GetComponent(typeof(TextMesh)) as TextMesh;
        textMesh.text = "Accelero:\nX: " + Input.acceleration.x.ToString("0.00") + "\nY: " + Input.acceleration.y.ToString("0.00") + "\nZ: " + Input.acceleration.z.ToString("0.00");
        textMesh.text += "\nGyroscope:\nX: " + Input.gyro.attitude.x.ToString("0.00") + "\nY: " + Input.gyro.attitude.y.ToString("0.00") + "\nZ: " + Input.gyro.attitude.z.ToString("0.00") + "\nW: " + Input.gyro.attitude.w.ToString("0.00");
        //textMesh.text += "\nMagneto:   X: " + Input.compass.rawVector.x.ToString("0.00") + "   Y: " + Input.compass.rawVector.y.ToString("0.00") + "   Z: " + Input.compass.rawVector.z.ToString("0.00");
        textMesh.text += string.Format("\n{0:0.0} ms ({1:0.} fps)", msec, fps);

    }
}
