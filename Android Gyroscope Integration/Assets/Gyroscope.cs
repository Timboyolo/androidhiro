﻿using UnityEngine;
using System.Collections;

public class Gyroscope : MonoBehaviour {

    /*
    Quaternion rotation;
    Quaternion negativeRotation;
    Quaternion negativeModifiedRotation;
    Quaternion modifiedRotation;
    Quaternion quatMult;
    Quaternion quatMap;
    Vector2 screenSize;
    GameObject camGrandparent;
    */
    void Awake()
    {
        /*
        // find the current parent of the camera's transform
        var currentParent = transform.parent;
        // instantiate a new transform
        var camParent = new GameObject("camParent");
        // match the transform to the camera position
        camParent.transform.position = transform.position;
        // make the new transform the parent of the camera transform
        transform.parent = camParent.transform;
        // instantiate a new transform
        camGrandparent = new GameObject("camGrandParent");
        // match the transform to the camera position
        camGrandparent.transform.position = transform.position;
        // make the new transform the grandparent of the camera transform
        camParent.transform.parent = camGrandparent.transform;
        // make the original parent the great grandparent of the camera transform
        camGrandparent.transform.parent = currentParent;

        camParent.transform.eulerAngles = new Vector3(-90, 0, 0);
        if (Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            quatMult = new Quaternion(0, 0, (float)0.7071, (float)-0.7071);
        }
        else if (Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            quatMult = new Quaternion(0, 0, (float)-0.7071, (float)-0.7071);
        }
        else if (Screen.orientation == ScreenOrientation.Portrait)
        {
            quatMult = new Quaternion(0, 0, 0, 1);
        }
        else if (Screen.orientation == ScreenOrientation.PortraitUpsideDown)
        {
            quatMult = new Quaternion(0, 0, 1, 0);
        }
        */
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Start()
    {
        //Input.compensateSensors = true;
        if (!Input.gyro.enabled)
        {
            Input.gyro.enabled = true;
        }
        //screenSize.x = Screen.width;
        //screenSize.y = Screen.height;

        //Camera.current.transform.eulerAngles = new Vector3(-90, 0, 0);
    }
    void Update()
    {
        //Camera.current.transform.Rotate(-Input.gyro.rotationRate.x, -Input.gyro.rotationRate.y, -Input.gyro.rotationRate.z);
        Camera.current.transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f) * new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);

        //quatMap = new Quaternion(Input.gyro.attitude.w, Input.gyro.attitude.x, Input.gyro.attitude.y, Input.gyro.attitude.z);
        //transform.localRotation = quatMap * quatMult;

        /*
        rotation = Input.gyro.attitude;
        negativeRotation = Quaternion.Inverse(rotation);
        negativeModifiedRotation = negativeRotation * Quaternion.AngleAxis(90, Vector3.right);
        modifiedRotation = rotation * Quaternion.AngleAxis(90, Vector3.right);
        //Z-as nog negatief maken inverse
        //#### CHOOSE WISELY ####
        Camera.current.transform.rotation = negativeModifiedRotation;
        //#### CHOOSE WISELY ####
        */
    }
}
