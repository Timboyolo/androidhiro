﻿using UnityEngine;
using System.Collections;

public class Magnetometer : MonoBehaviour {
    float compassX;
    float compassY;
    float compassZ;
    Quaternion rotation;
    void Start () {
        Input.location.Start();
        Input.compass.enabled = true;
    }
	
	// Update is called once per frame
	void Update () {
        compassX = Input.compass.rawVector.x*2;
        compassY = Input.compass.rawVector.y*2;
        compassZ = Input.compass.rawVector.z*2;
        rotation = Quaternion.Euler(-compassX, -compassY, -compassZ);
        Camera.current.transform.rotation = rotation;
    }
}
