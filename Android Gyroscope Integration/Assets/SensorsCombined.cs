﻿using UnityEngine;
using System.Collections;

public class SensorsCombined : MonoBehaviour {
    public float thrust = 0.01f;
    private Transform cameraTransform;
    void Awake() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
	// Use this for initialization
	void Start () {
        if (!Input.gyro.enabled)
        {
            Input.gyro.enabled = true;
        }
    }
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(-Input.acceleration.x, -Input.acceleration.y, -Input.acceleration.z);
        transform.rotation = Quaternion.Euler(90.0f, 0.0f, 0.0f) * new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
        if (Input.anyKey)
        {
            transform.position += transform.forward * thrust * Time.deltaTime;
        }
    }
}
